sudo pacman -Syu
sudo pacman base-devel git
sudo pacman fwupd fwupd-efi flatpak libpamac-flatpak-plugin gnome-firmware intel-ucode system-config-printer
sudo pacman keepassxc brave-browser lollypop gtk-engine-murrine gtk-engines
flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
flatpak install flathub com.Spotify.Client
flatpak install flathub org.onlyoffice.desktopeditors
